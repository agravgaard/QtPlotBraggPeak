#ifndef PARABOLICCYLINDER_H
#define PARABOLICCYLINDER_H

#include <cmath>

#include "constants.h"

/*
  Adopted from Randy Read's code by Pavel Afonine, 12-MAR-2014
*/

constexpr double dvsa(double, double);
constexpr double dvla(double, double);
constexpr double vvla(double, double);

constexpr double dv(double va, double x) {
  /* Compute parabolic cylinder function Dv(x)
     Equivalent to Mathematica ParabolicCylinderD[va,x]

     Derived from routines in program mpbdv.for by Shanjie Zhang and Jianming
     Jin distributed with their book "Computation of Special Functions",
     Copyright 1996 by John Wiley & Sons, Inc.
     Permission has been granted for purchasers of the book to incorporate these
     programs as long as the copyright is acknowledged.

     va = order of the parabolic cylinder function Dv
     x  = argument
  */
  double ax(std::abs(x));
  if (ax <= 5.8)
    return dvsa(va, x);
  else
    return dvla(va, x);
}

constexpr double dvsa(const double va, const double x) {
  // Compute parabolic cylinder function Dv(x) for small values of |x| (<=5.8)
  constexpr double EPS = 10e-15;                      // (std::pow(10., -15));
  constexpr double SQRT2 = scitbx::constants::sqrt_2; // (std::sqrt(2.));
  constexpr double SQRTPI = scitbx::constants::sqrt_pi;
  double ep = std::exp(static_cast<long double>(-0.25 * x * x));
  double va0 = 0.5 * (1. - va);
  if (va == 0.)
    return ep;
  else {
    if (x == 0.) {
      double ftol = std::numeric_limits<double>::epsilon();
      if (va0 <= 0. && std::abs(va0 - std::floor(va0 + 0.5)) < ftol)
        return 0.;
      else
        return SQRTPI / (std::tgamma(va0) * std::pow(2, -0.5 * va));
    } else {
      double a0 = std::pow(2, -0.5 * va - 1.) * ep / std::tgamma(-va);
      double vt = -0.5 * va;
      auto pd = std::tgamma(vt);
      double r(1.), r1(pd);
      int m(1);
      while (m <= 250 && std::abs(r1) >= std::abs(pd) * EPS) {
        double vm = 0.5 * (m - va);
        r = -r * SQRT2 * x / m;
        r1 = std::tgamma(vm) * r;
        pd += r1;
        m++;
      }
      pd *= a0;
      return pd;
    }
  }
}

constexpr double dvla(const double va, const double x) {
  // Compute parabolic cylinder function Dv(x) for large values of |x| (>5.8)
  constexpr double EPS = 10e-12; // (std::pow(10., -12));
  double xsqr(x * x);
  double ep(std::exp(static_cast<long double>(-0.25 * xsqr)));
  double a0(std::pow(std::abs(x), static_cast<long double>(va)) * ep);
  double r(1.);
  double pd(1.);
  int k(1);
  while (k <= 16 && std::abs(r / pd) >= EPS) {
    r = -0.5 * r * (2. * k - va - 1.) * (2. * k - va - 2.) / (k * xsqr);
    pd += r;
    k++;
  }
  pd *= a0;
  if (x < 0.) {
    double x1(-x);
    double vl = vvla(va, x1);
    pd = scitbx::constants::pi * vl / std::tgamma(-va) +
         cos(scitbx::constants::pi * va) * pd;
  }
  return pd;
}

constexpr double vvla(const double va, const double x) {
  // Compute parabolic cylinder function Vv(x) for large argument
  constexpr double EPS = 10e-12; // (std::pow(10., -12));
  constexpr double SQRT2BYPI =
      scitbx::constants::sqrt_2bypi; // (std::sqrt(2. / scitbx::constants::pi));
  double xsqr(x * x);
  double qe(std::exp(static_cast<long double>(0.25 * xsqr)));
  double a0 = std::pow(std::abs(x), static_cast<long double>(-va - 1.)) *
              SQRT2BYPI * qe;
  double r(1.), pv(1.);
  int k(1);
  while (k <= 18 && std::abs(r / pv) >= EPS) {
    r = 0.5 * r * (2. * k + va - 1.) * (2. * k + va) / (k * xsqr);
    pv += r;
    k++;
  }
  pv *= a0;
  if (x < 0.) {
    double x1(-x);
    double pdl = dvla(va, x1);
    double gl = std::tgamma(-va);
    double piva = scitbx::constants::pi * va;
    double dsl = std::pow(sin(piva), 2);
    pv = dsl * gl / scitbx::constants::pi * pdl - cos(piva) * pv;
  }
  return pv;
}

#endif // PARABOLICCYLINDER_H
