"""Script to fit exponential function to proton range data"""
import numpy as np
from scipy.optimize import curve_fit as cf
from matplotlib import pyplot as plt

def power_fun(x, a, b):
    """Function to fit"""
    return a * pow(x, b)

def main():
    """Main"""
    data = np.loadtxt("data_air_proton_range.txt", skiprows=1)
    energy = data[:115, 0]
    c_range = data[:115, 4]

    popt,_ = cf(power_fun, energy, c_range)
    plt.plot(energy, c_range, 'x')
    plt.plot(energy, power_fun(energy, popt[0], popt[1]))
    plt.show()

    print(popt)


if __name__ == "__main__":
    main()
